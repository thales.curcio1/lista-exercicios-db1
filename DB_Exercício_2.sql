-- Criação do banco de dados
CREATE DATABASE ficha_medica_paciente;

-- Selecionar o banco de dados
USE ficha_medica_paciente;

-- Criar tabela para armazenar as informações do paciente
CREATE TABLE ficha_medica (
    id_paciente INT AUTO_INCREMENT PRIMARY KEY;
    nome VARCHAR(100) NOT NULL,
    data_nascimento DATE,
    sexo CHAR, -- Campo de informação de um único caractére (M para masculino, F para feminino)
    convenio VARCHAR (100),
    estado_civil VARCHAR(20),
    rg VARCHAR (15), -- Se não houver operações matemáticas, VARCHAR é o suficiente
    telefone VARCHAR(20),
    endereco VARCHAR(255),
);

-- Criar tabela para armazenar o histórico de consultas do paciente
CREATE TABLE consultas (
    id_paciente INT AUTO_INCREMENT PRIMARY KEY,
    CONSTRAINT fk_fichaconsulta FOREIGN KEY (id_paciente) REFERENCES ficha_medica (id_paciente),
    num_consulta INT,
    data_consulta DATE,
    nome_medico VARCHAR (100), -- nome do médico responsável pela consulta ou paciente
    diagnostico TEXT
);

-- Criar tabela para armazenar o histórico de exames do paciente
CREATE TABLE exames (
    id_paciente INT AUTO_INCREMENT PRIMARY KEY,
    CONSTRAINT fk_fichaexame FOREIGN KEY (id_paciente) REFERENCES ficha_medica (id_paciente),
    num_exame INT,
    tipo_exame VARCHAR (100),
    data_exame DATE,
    resultado TEXT
);