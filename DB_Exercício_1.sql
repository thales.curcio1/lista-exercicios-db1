-- Criação do banco de dados
CREATE DATABASE dados_cadastrais_funcionario;

-- Selecionar o banco de dados
USE dados_cadastrais_funcionario;

-- Criar tabela para armazenar os dados dos funcionários
CREATE TABLE funcionarios (
    id_funcionrio INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(100) NOT NULL,
    data_nascimento DATE,
    nacionalidade VARCHAR(50),
    sexo CHAR, -- Campo de informação de um único caractére (M para masculino, F para feminino)
    estado_civil VARCHAR(20),
    rg VARCHAR (15), -- Se não houver operações matemáticas, VARCHAR é o suficiente.
    cpf VARCHAR (11),
    telefone VARCHAR(20),
    endereco VARCHAR(255),
    cargo VARCHAR(100),
    salario DECIMAL(10, 2),
    data_admissao DATE
);

-- Criar tabela para armazenar os cargos anteriormente ocupados pelo funcionário
CREATE TABLE cargos_ocupados (
    id_funcionrio INT AUTO_INCREMENT PRIMARY KEY,
    CONSTRAINT fk_funcargo FOREIGN KEY (id_funcionrio) REFERENCES funcionarios (id_funcionrio),
    cargo1 VARCHAR (100),
    data_inicio DATE,
    data_final DATE,
    cargo2 VARCHAR (100),
    data_inicio1 DATE,
    data_final2 DATE
);

-- Criar tabela para armazenar o departamento que o funcionário ira exercer sua função.
CREATE TABLE depto_lotacao (
    id_funcionrio INT AUTO_INCREMENT PRIMARY KEY,
    CONSTRAINT fk_fundepto FOREIGN KEY (id_funcionrio) REFERENCES funcionarios (id_funcionrio),
    depto VARCHAR (100),
    dt_inicial DATE,
    dt_final DATE
    depto2 VARCHAR (100),
    dt_inicial2 DATE,
    dt_final2 DATE
);

-- Criar tabela para armazenar dependentes registrados pelo funcionário.
CREATE TABLE dependentes (
    id_funcionrio INT AUTO_INCREMENT PRIMARY KEY,
    CONSTRAINT fk_fundependentes FOREIGN KEY (id_funcionrio) REFERENCES funcionarios (id_funcionrio),
    nome_dependente1 VARCHAR (100),
    data_nascimento1 DATE,
    nome_dependente2 VARCHAR (100),
    data_nascimento2 DATE
);