-- Criação do banco de dados
CREATE DATABASE escola;

-- Selecionar o banco de dados
USE escola;

-- Criar tabela para armazenar os dados dos alunos
CREATE TABLE alunos_inscricao (
    ra_aluno INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(100) NOT NULL,
    data_nascimento DATE,
    idade INT,
    nacionalidade VARCHAR(50),
    sexo CHAR, -- Campo de informação de um único caractére (M para masculino, F para feminino)
    telefone VARCHAR(20),
    endereco VARCHAR(255),
);

CREATE TABLE cursos(
    ra_aluno INT AUTO_INCREMENT PRIMARY KEY,
    CONSTRAINT fk_alunocurso FOREIGN KEY (ra_aluno) REFERENCES alunos_inscricao (ra_aluno),
    cod_curso INT,
    nome_curso VARCHAR(150)
);

